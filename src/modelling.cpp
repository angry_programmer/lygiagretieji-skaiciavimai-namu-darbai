//
//  modelling.c
//  MBD-S-1
//
//  Created by Gintaras Dreizas on 31/03/14.
//  Copyright (c) 2014 MIF. All rights reserved.
//

#include "parameters.h"
#include "modelling.h"
#include "Header.h"

void shift_arrays();
void shift_tau_values();
void shift_tau_values2();
void shift_I_values();
void shift_dI_values();

void print_array(char *name, Real *a, int offset, int len)
{
    for (int i = offset; i < len; i++)
    {
        printf("%s[%i] = %.30lf\n", name, i, a[i]);
    }
}

void progress(Real *a, Real *b, Real *result, int offset, int size)
{
    Real r = 0.0;
    for (int i = offset; i < size; i++)
    {
        Real s = fabs(a[i] - b[i]);
        r = fmax(s, r);
    }
    (*result) = r;
}

void max_element(Real *a, Real *result, int offset, int size)
{
    Real r = 0.0;
    for (int i = offset; i < size; i++)
    {
        r = fmax(a[i], r);
    }
    (*result) = r;
}

Modeller::Modeller() : prod_diffusion_through_outer_membrane(false)
{
    this->_init_arrays();
}

Modeller::Modeller(bool prod_diffusion_through_outer_membrane) : prod_diffusion_through_outer_membrane(prod_diffusion_through_outer_membrane)
{
    this->_init_arrays();
}

Modeller::~Modeller()
{
    this->_clean_up_arrays();
    this->_destroy_arrays();
}

void Modeller::_init_arrays()
{
    _array_sz = sizeof(Real) * (params.N + 1);

    s_prev = (Real *)malloc(this->_array_sz);
    p_prev = (Real *)malloc(this->_array_sz);
    e_prev = (Real *)malloc(this->_array_sz);

    s_array = (Real *)malloc(this->_array_sz);
    p_array = (Real *)malloc(this->_array_sz);
    e_array = (Real *)malloc(this->_array_sz);

    s_new = (Real *)malloc(this->_array_sz);
    p_new = (Real *)malloc(this->_array_sz);
    e_new = (Real *)malloc(this->_array_sz);

    s_old = (Real *)malloc(this->_array_sz);
    p_old = (Real *)malloc(this->_array_sz);
    e_old = (Real *)malloc(this->_array_sz);

    alpha = (Real *)malloc(this->_array_sz);
    beta = (Real *)malloc(this->_array_sz);

    a = (Real *)malloc(this->_array_sz);
    b = (Real *)malloc(this->_array_sz);
    c = (Real *)malloc(this->_array_sz);
    f = (Real *)malloc(this->_array_sz);
    z = (Real *)malloc(this->_array_sz);
    A = (Real *)malloc(this->_array_sz);
    B = (Real *)malloc(this->_array_sz);
    Y = (Real *)malloc(this->_array_sz);
}

void Modeller::_clean_up_arrays()
{
    this->_clean_up_prev_arrays();
    this->_clean_up_current_arrays();
    this->_clean_up_new_arrays();
    this->_clean_up_old_arrays();
    this->_clean_up_abcfz_arrays();
    this->_clean_up_ABY_arrays();
    this->_clean_up_tdma_arrays();
}

void Modeller::_clean_up_prev_arrays()
{
    memset(s_prev, 0x0, this->_array_sz);
    memset(p_prev, 0x0, this->_array_sz);
    memset(e_prev, 0x0, this->_array_sz);
}

void Modeller::_clean_up_current_arrays()
{
    memset(s_array, 0x0, this->_array_sz);
    memset(p_array, 0x0, this->_array_sz);
    memset(e_array, 0x0, this->_array_sz);
}

void Modeller::_clean_up_new_arrays()
{
    memset(s_new, 0x0, this->_array_sz);
    memset(p_new, 0x0, this->_array_sz);
    memset(e_new, 0x0, this->_array_sz);
}

void Modeller::_clean_up_old_arrays()
{
    memset(s_old, 0x0, this->_array_sz);
    memset(p_old, 0x0, this->_array_sz);
    memset(e_old, 0x0, this->_array_sz);
}

void Modeller::_clean_up_abcfz_arrays()
{
    memset(a, 0x0, this->_array_sz);
    memset(b, 0x0, this->_array_sz);
    memset(c, 0x0, this->_array_sz);
    memset(f, 0x0, this->_array_sz);
    memset(z, 0x0, this->_array_sz);
}

void Modeller::_clean_up_ABY_arrays()
{
    memset(A, 0x0, this->_array_sz);
    memset(B, 0x0, this->_array_sz);
    memset(Y, 0x0, this->_array_sz);
}

void Modeller::_clean_up_tdma_arrays()
{
    memset(alpha, 0x0, this->_array_sz);
    memset(beta, 0x0, this->_array_sz);
}

void Modeller::_destroy_arrays()
{
    if (s_prev != NULL)
    {
        free(s_prev);
        s_prev = NULL;
    }
    if (p_prev != NULL)
    {
        free(p_prev);
        p_prev = NULL;
    }
    if (e_prev != NULL)
    {
        free(e_prev);
        e_prev = NULL;
    }
    if (s_array != NULL)
    {
        free(s_array);
        s_array = NULL;
    }
    if (p_array != NULL)
    {
        free(p_array);
        p_array = NULL;
    }
    if (e_array != NULL)
    {
        free(e_array);
        e_array = NULL;
    }
    if (s_new != NULL)
    {
        free(s_new);
        s_new = NULL;
    }
    if (p_new != NULL)
    {
        free(p_new);
        p_new = NULL;
    }
    if (e_new != NULL)
    {
        free(e_new);
        e_new = NULL;
    }
    if (s_old != NULL)
    {
        free(s_old);
        s_old = NULL;
    }
    if (p_old != NULL)
    {
        free(p_old);
        p_old = NULL;
    }
    if (e_old != NULL)
    {
        free(e_old);
        e_old = NULL;
    }
    if (alpha != NULL)
    {
        free(alpha);
        alpha = NULL;
    }
    if (beta != NULL)
    {
        free(beta);
        beta = NULL;
    }
    if (a != NULL)
    {
        free(a);
        a = NULL;
    }
    if (b != NULL)
    {
        free(b);
        b = NULL;
    }
    if (c != NULL)
    {
        free(c);
        c = NULL;
    }
    if (f != NULL)
    {
        free(f);
        f = NULL;
    }
    if (z != NULL)
    {
        free(z);
        z = NULL;
    }
    if (A != NULL)
    {
        free(A);
        A = NULL;
    }
    if (B != NULL)
    {
        free(B);
        B = NULL;
    }
    if (Y != NULL)
    {
        free(Y);
        Y = NULL;
    }
}

void Modeller::run(const char *base_path, Real result[2])
{

    sprintf(current_path, "%s/produktas_%spatenka_i_aplinka/", base_path, (prod_diffusion_through_outer_membrane ? "" : "ne"));

    last_i = 0.0;
    last_di = 0.0;

    tau_min = 1e-14;
    for (int i = 0; i < params.N + 1; i++)
    {
        s_array[i] = S(i);
        p_array[i] = 0.0;
        e_array[i] = E(i);
    }

    _I = calc_I(p_array);
    shift_I_values();
    printToCSV();

    tau_prev = 0.0;
    tau = tau_min;
    tau_next = tau_min * q_max;

    k++;
    solve_system();
    shift_arrays();
    _I = calc_I(p_array);
    if (_I < _I_prev)
    {
        printToCSV();
        printf("%s:%s:%i - Klaida!", __FILE__, __FUNCTION__, __LINE__);
        exit(EXIT_FAILURE);
    }

    _dI = (_I - _I_prev) / tau;
    if (_dI < 0.0)
    {
        printToCSV();
        printf("%s:%s:%i - Klaida!", __FILE__, __FUNCTION__, __LINE__);
        exit(EXIT_FAILURE);
    }

    t = tau;
    shift_I_values();
    shift_dI_values();
    shift_tau_values();
    progress(s_array, s_prev, &max_dS_dt_curr, 0, params.N + 1);
    max_dS_dt_curr /= tau;
    max_dS_dt_prev = max_dS_dt_curr;

    printToCSV();

    Real tau_max_sq = pow(params.tau_max, 2.0);
    while (t < tau_max_sq)
    {
        k++;
        solve_system();
        shift_arrays();
        _I = calc_I(p_array);

        if (_I < _I_prev)
        {
            printToCSV();
            printf("%s:%s:%i - Klaida!", __FILE__, __FUNCTION__, __LINE__);
            exit(EXIT_FAILURE);
        }

        Real x[3] = {
            t - tau,
            t,
            t + tau,
        };
        Real u[3] = {
            _I_prev2,
            _I_prev,
            _I,
        };
        _dI = calc_dI(t, x, u);
        if (_dI < 0.0)
        {
            printToCSV();
            printf("%s:%s:%i - Klaida! Iteracija [ %li ].", __FILE__, __FUNCTION__, __LINE__, k);
            exit(EXIT_FAILURE);
        }

        if (_dI < _dI_prev)
        {
            printToCSV();
            printf("%s:%s:%i - Klaida!Iteracija [ %li ].", __FILE__, __FUNCTION__, __LINE__, k);
            exit(EXIT_FAILURE);
        }

        printToCSV();

        progress(s_array, s_prev, &max_dS_dt_curr, 0, params.N + 1);
        max_dS_dt_curr /= tau;
        max_dS_dt_prev = max_dS_dt_curr;

        t += tau;
        shift_tau_values();
        shift_I_values();
        shift_dI_values();
    }

    Real e_current_settling_crit = 1e-3;
    int e_current_is_settled = 0;
    int t_slopeMAX_is_found = 0;
    int should_continue_when_t_slopeMAX_is_found = 1;
    int should_continue = 1;
    const Real significant_density_value = DBL_EPSILON * 100.0;
    while (t < params.T && (should_continue == 1))
    {
        k++;
        solve_system();
        shift_arrays();
        _I = calc_I(p_array);
        if (_I < _I_prev && _I > significant_density_value)
        {
            printToCSV();
            printf("%s:%s:%i - Klaida! Iteracija [ %li ].", __FILE__, __FUNCTION__, __LINE__, k);
            exit(EXIT_FAILURE);
        }

        Real x[3] = {
            t - tau,
            t,
            t + tau,
        };
        Real u[3] = {
            _I_prev2,
            _I_prev,
            _I,
        };
        _dI = calc_dI(t, x, u);
        if (_dI < 0.0)
        {
            printToCSV();
            printf("%s:%s:%i - Klaida! Iteracija [ %li ].", __FILE__, __FUNCTION__, __LINE__, k);
            exit(EXIT_FAILURE);
        }

        if (_dI < _dI_prev && _dI_prev > significant_density_value && t_slopeMAX_is_found == 0)
        {
            t_slopeMAX_is_found = 1;
            t_slopeMAX = t - tau_prev;
            max_dI = _dI_prev;
        }

        if (t_slopeMAX_is_found == 1 && t > q_max * t_slopeMAX && should_continue_when_t_slopeMAX_is_found == 0)
        {
            should_continue = 0;
        }

        t += tau;
        shift_tau_values();
        shift_I_values();
        shift_dI_values();

        if (t_slopeMAX_is_found == 1 && t + tau > q_max * t_slopeMAX && (_dI / max_dI) < e_current_settling_crit)
        {
            e_current_is_settled = 1;
            should_continue = 0;

            Real enzyme_avg = 0.0;
            long nnz = 0;
            int i;
            for (i = 0; i < params.N + 1; i++)
            {
                enzyme_avg += e_array[i];
                if (e_array[i] > 0.0)
                {
                    nnz++;
                }
            }
            enzyme_avg /= (Real)nnz;
            params.Vmax = params.k_plus_3 * (params.E0 - enzyme_avg);
            result[0] = t;
            result[1] = _I;
            printToCSV();
            break;
        }

        //        printToCSV();
    }

    tau_prev = 0.0;
    tau = 0.0;
    tau_next = 0.0;

    t = 0.0;

    max_dS_dt_prev = 0.0;
    max_dS_dt_curr = 0.0;

    k = 0;
    t_slopeMAX = -1.0;

    _I_prev = 0.0;
    _I_prev2 = 0.0;
    _I = 0.0;

    _dI_prev = 0.0;
    _dI_prev2 = 0.0;
    _dI = 0.0;

    max_dI = 0.0;

    last_i = 0.0;
    last_di = 0.0;

    tau_min = 0.0;
} // end of run()

void Modeller::shift_tau_values()
{
    tau_prev = tau;
    if (tau == params.tau_max)
    {
        return;
    }
    tau *= q_max;
    if (tau > params.tau_max)
    {
        tau = params.tau_max;
    }
}

void Modeller::shift_tau_values2()
{
    tau_prev = tau;
    tau = tau_next;
}

void Modeller::shift_I_values()
{
    _I_prev2 = _I_prev;
    _I_prev = _I;
}

void Modeller::shift_dI_values()
{
    _dI_prev2 = _dI_prev;
    _dI_prev = _dI;
}

void Modeller::shift_arrays()
{
    arr_cpy(&s_prev, &s_array, params.N + 1);
    arr_cpy(&p_prev, &p_array, params.N + 1);
    arr_cpy(&e_prev, &e_array, params.N + 1);

    arr_cpy(&s_array, &s_new, params.N + 1);
    arr_cpy(&p_array, &p_new, params.N + 1);
    arr_cpy(&e_array, &e_new, params.N + 1);

    //    {
    //        max_dS_dt_prev = max_dS_dt_curr;
    //        max_dS_dt_curr = 0.0;
    //        Real tmp = 0.0;
    //        int i;
    //        for (i = 0; i < N+1; i++)
    //        {
    //            tmp = fabs(s_array[i] - s_prev[i]) / tau;
    //            max_dS_dt_curr = max(tmp, max_dS_dt_curr);
    //        }
    //    }
}

void Modeller::solve_system()
{
    Real delta = DBL_EPSILON * 100.0;

    arr_cpy(&s_old, &s_array, params.N + 1);
    arr_cpy(&p_old, &p_array, params.N + 1);
    arr_cpy(&e_old, &e_array, params.N + 1);

    int counter = 0;
    bool canCompare = false;
    while (true)
    {
        if (counter >= 10000)
        {
            printf("%s:%s:%i - Pasiektas maksimalus leistinas iteraciju skaicius!", __FILE__, __FUNCTION__, __LINE__);
            exit(EXIT_FAILURE);
        }
        this->_clean_up_new_arrays();

        solve_system_S(e_old, s_new);
        solve_system_P(e_old, p_new);

        for (int i = 0; i < params.N + 1; i++)
        {
            Real top = (2.0 / tau) * e_array[i] + Alpha(x(i, 0)) * (params.k_minus_1 + params.k_plus_3) * params.E0;
            Real bottom = (2.0 / tau) + Alpha(x(i, 0)) * (params.k_plus_1 * s_new[i] + params.k_minus_3 * p_new[i] + params.k_minus_1 + params.k_plus_3);
            e_new[i] = (top / bottom);
        }

        Real s_max = 0.0, s_new_max = 0.0;
        Real p_max = 0.0, p_new_max = 0.0;
        Real e_max = 0.0, e_new_max = 0.0;

        // Step 6
        {
            //            for (int i = 0; i < N+1; i++) {
            //                if (s_new[i] >= 0.0 && s_new[i] <= S0) { /* ok */ } else { printf("Step 6: \n\ts_new[%d] = %.8f doesn't fit in bounds [0.0, %.8f]\n\n", i, s_new[i], S0); exit(-1); }
            //                if (p_new[i] >= 0.0) { /* ok */ }
            //                else
            //                {
            //                    printf("Step 6: \n\tp_new[%d] = %.8f doesn't fit in bounds [0.0, ...\n\n", i, p_new[i]);
            //                    exit(-1);
            //                }
            //
            //                assert(e_new[i] >= 0.0);
            //                if (e_new[i] > (E0 + DBL_EPSILON))
            //                {
            //                    printf("Step 6: \n\te_new[%d] = %.8f doesn't fit in bounds [0.0, %.8f]\n\n", i, e_new[i], E0);
            //                    exit(-1);
            //                }
            //            }

            max_element(s_array, &s_max, 0, params.N);
            max_element(s_new, &s_new_max, 0, params.N);
            //            if (s_max < s_new_max) { /* ok */} else { printf("Step 6:\n\tMaximum S value [%.8f] is greater than or equal to maximum of a new S [%.8f]\n\n", s_max, s_new_max); exit(-1); }

            max_element(p_array, &p_max, 0, params.N + 1);
            max_element(p_new, &p_new_max, 0, params.N + 1);
            //            if (p_max <= p_new_max) { /* ok */ } else { printf("Step 6: \n\tMaximum P value [%.8f] is greater than maximum of a new P [%.8f]\n\n", p_max, p_new_max); exit(-1); }

            max_element(e_array, &e_max, 0, params.N + 1);
            max_element(e_new, &e_new_max, 0, params.N + 1);
            //            assert(e_max >= (e_new_max - DBL_EPSILON));
            //            if (e_new_max < delta)
            //            {
            //                printf("Step 6: \n\tMaximum E value [%.8f] is less than maximum of a new E [%.8f]\n\n", e_max, e_new_max);
            //                exit(-1);
            //            }
        }

        if (canCompare)
        {
            Real s_progress = 0.0, p_progress = 0.0, e_progress = 0.0;
            Real s_residual = 0.0, p_residual = 0.0;

            // Step 7
            {
                progress(s_new, s_array, &max_dS_dt_curr, 0, params.N + 1);
                progress(s_new, s_old, &s_progress, 0, params.N + 1);
                progress(p_new, p_old, &p_progress, 0, params.N + 1);
                progress(e_new, e_old, &e_progress, 0, params.N + 1);

                Real s_residual_array[params.N + 1], p_residual_array[params.N + 1];
                for (int i = 1; i < params.N; i++)
                {
                    Real sr_top = absolute(S_a(i) * s_new[i - 1] - S_c(e_new[i], i) * s_new[i] + S_b(i) * s_new[i + 1] + S_f(e_new[i], i));
                    Real sr_bottom = absolute(S_a(i)) + absolute(S_b(i)) + absolute(S_c(e_new[i], i));
                    s_residual_array[i] = sr_top / sr_bottom;

                    Real pr_top = absolute(P_a(i) * p_new[i - 1] - P_c(e_new[i], i) * p_new[i] + P_b(i) * p_new[i + 1] + P_f(e_new[i], i));
                    Real pr_bottom = absolute(P_a(i)) + absolute(P_b(i)) + absolute(P_c(e_new[i], i));
                    p_residual_array[i] = pr_top / pr_bottom;
                }
                max_element(s_residual_array, &s_residual, 1, params.N);
                max_element(p_residual_array, &p_residual, 1, params.N);
            }

            // Step 8
            {
                if (s_progress < delta * s_new_max)
                {
                    if (p_progress <= delta * p_new_max)
                    {
                        if (e_progress < delta * e_new_max)
                        {
                            if (s_residual < delta * s_new_max)
                            {
                                if (p_residual <= delta * p_new_max)
                                {
                                    break;
                                }
                                else
                                {
#ifdef DEBUG_LOG
                                    fprintf(stderr, "Step 8:\n\tP netiktis perdidelė %.8f > %.8f\n\n", p_residual, delta * p_new_max);
#endif
                                }
                            }
                            else
                            {
#ifdef DEBUG_LOG
                                fprintf(stderr, "Step 8:\n\tS netiktis perdidelė %.8f > %.8f\n\n", s_residual, delta * s_new_max);
#endif
                            }
                        }
                        else
                        {
#ifdef DEBUG_LOG
                            fprintf(stderr, "Step 8:\n\tE progresas perdidelis %.8f > %.8f\n\n", e_progress, delta * e_new_max);
#endif
                        }
                    }
                    else
                    {
#ifdef DEBUG_LOG
                        fprintf(stderr, "Step 8:\n\tP progresas perdidelis %.8f > %.8f\n\n", p_progress, delta * p_new_max);
#endif
                    }
                }
                else
                {
#ifdef DEBUG_LOG
                    fprintf(stderr, "Step 8:\n\tS progresas perdidelis %.8f > %.8f\n\n", s_progress, delta * s_new_max);
#endif
                }
            }
        }

        arr_cpy(&s_old, &s_new, params.N + 1);
        arr_cpy(&p_old, &p_new, params.N + 1);
        arr_cpy(&e_old, &e_new, params.N + 1);
        canCompare = true;
        counter++;
    }

    this->_clean_up_old_arrays();
}

void Modeller::solve_system_S(Real *e_old, Real *new_array)
{
    this->_clean_up_abcfz_arrays();
    for (int i = 0; i < params.N + 1; i++)
    {
        *(a + i) = S_a(i);
        *(b + i) = S_b(i);
        *(c + i) = S_c(*(e_old + i), i);
        *(f + i) = S_f(*(e_old + i), i);
        *(z + i) = 0.0;
    }

    this->_clean_up_ABY_arrays();
    for (int i = 0; i < params.N + 1; i++)
    {
        *(A + i) = 0.0;
        *(B + i) = 0.0;
        *(Y + i) = 0.0;
    }
    tdma(a, b, c, z, 0, 1, B);
    tdma(a, b, c, z, 1, 0, A);
    tdma(a, b, c, f, 0, 0, Y);

    Real top = (4.0 * (params.S0 * B[1] + Y[1]) - params.S0 * B[2] - Y[2]);
    Real bottom = (3.0 - 4.0 * A[1] + A[2]);
    new_array[0] = top / bottom;
    new_array[params.N] = params.S0;

    for (int i = 0; i < params.N + 1; i++)
    {
        new_array[i] = fabs(new_array[0] * A[i] + new_array[params.N] * B[i] + Y[i]);
        new_array[i] = new_array[params.N] <= DBL_EPSILON ? 0.0 : new_array[i];
    }
}

void Modeller::solve_system_P(Real *e_old, Real *new_array)
{
    /*
     jeigu isorine membrana pralaidi produktui --> perkelties_metodas_Dirichle0_Dirichle
     priesingu atveju --> perkelties_metodas_adaptuotas_Dirichle0_Noimano0
     */
    this->_clean_up_abcfz_arrays();
    for (int i = 0; i < params.N + 1; i++)
    {
        a[i] = P_a(i);
        b[i] = P_b(i);
        c[i] = P_c(e_old[i], i);
        f[i] = P_f(e_old[i], i);
        z[i] = 0.0;
    }

    if (prod_diffusion_through_outer_membrane)
    {
        tdma2(a, b, c, f, new_array);
        return;
    }

    this->_clean_up_ABY_arrays();
    for (int i = 0; i < params.N + 1; i++)
    {
        *(B + i) = 0.0;
        *(Y + i) = 0.0;
    }
    tdma(a, b, c, z, 0.0, 1.0, B);
    tdma(a, b, c, f, 0.0, 0.0, Y);

    new_array[0] = 0.0;
    new_array[params.N] = (4.0 * Y[params.N - 1] - Y[params.N - 2]) / (3.0 - 4.0 * B[params.N - 1] + B[params.N - 2]);
    new_array[params.N] = new_array[params.N] < DBL_EPSILON ? 0.0 : new_array[params.N];

    for (int i = 0; i < params.N + 1; i++)
    {
        //        new_array[i] = new_array[N] * B[i] + Y[i];
        new_array[i] = new_array[params.N] <= DBL_EPSILON ? fabs(Y[i]) : fabs(new_array[params.N] * B[i] + Y[i]);
    }
}

void Modeller::tdma(Real *a, Real *b, Real *c, Real *f, Real r0, Real rn, Real *result)
{

    alpha[1] = 0.0;
    beta[1] = r0;
    Real m = 0.0;
    int i;
    for (i = 1; i < params.N; i++)
    {
        m = 1.0 / (c[i] - alpha[i] * a[i]);
        alpha[i + 1] = b[i] * m;
        beta[i + 1] = (a[i] * beta[i] + f[i]) * m;
    }

    result[params.N] = rn;
    for (i = params.N; i-- > 0;)
    {
        result[i] = alpha[i + 1] * result[i + 1] + beta[i + 1];
    }
    result[0] = r0;
    this->_clean_up_tdma_arrays();
}

void Modeller::tdma2(Real *a, Real *b, Real *c, Real *f, Real *result)
{
    /*
     jeigu isorine membrana pralaidi produktui --> tdma2
     priesingu atveju --> tdma
     */
    alpha[1] = b[1] / c[1];
    beta[1] = f[1] / c[1];

    Real m = 0.0;
    int i;
    for (i = 2; i < params.N; i++)
    {
        m = 1.0 / (c[i] - a[i] * alpha[i - 1]);
        alpha[i] = b[i] * m;
        beta[i] = (f[i] + a[i] * beta[i - 1]) * m;
    }

    result[params.N] = f[params.N];
    for (i = params.N; i-- > 0;)
    {
        result[i] = alpha[i] * result[i + 1] + beta[i];
    }
    result[0] = 0.0;
    this->_clean_up_tdma_arrays();
}

Real Modeller::S(size_t i)
{
    if (i == params.N)
        return params.S0;
    return 0.0;
}
Real Modeller::P(size_t i) { return 0.0; }
Real Modeller::E(size_t i)
{
    Real xi = x(i, 0);
    if (params.Dm1 <= xi && xi <= params.Dm1 + params.De)
        return params.E0;
    return 0.0;
}

Real Modeller::S_c(Real e_old, size_t i)
{
    Real sa = S_a(i);
    Real sb = S_b(i);
    Real twodt = 2.0 / tau;
    Real alp = Alpha(x(i, 0));
    Real res = sa + sb + twodt + params.C1 + alp * params.k_plus_1 * (e_old / 2.0);
    return res;
}

Real Modeller::S_f(Real e_old, size_t i)
{
    Real res = (2.0 / tau) * s_array[i] + Alpha(x(i, 0)) * params.k_minus_1 * params.E0 * (1.0 - (e_old / params.E0));
    return (res > DBL_EPSILON ? res : 0.0);
}

Real Modeller::P_c(Real e_old, size_t i)
{
    Real res = P_a(i) + P_b(i) + (2.0 / tau) + params.C2 + Alpha(x(i, 0)) * params.k_minus_3 * (e_old / 2.0);
    return res;
}

Real Modeller::P_f(Real e_old, size_t i)
{
    Real res = (2.0 / tau) * p_array[i] + Alpha(x(i, 0)) * params.k_plus_3 * params.E0 * (1.0 - (e_old / params.E0));
    return (res > DBL_EPSILON ? res : 0.0);
}

void Modeller::E_(Real *e_new, Real e_old, Real s_old, Real s_new, Real p_old, Real p_new, size_t i)
{
    Real top = (2.0 / tau) * e_array[i] + Alpha(x(i, 0)) * (params.k_minus_1 + params.k_plus_3) * params.E0;
    Real bottom = (2.0 / tau) + Alpha(x(i, 0)) * (params.k_plus_1 * ((s_old + s_new) / 2.0) + params.k_minus_3 * ((p_old + p_new) / 2.0) + params.k_minus_1 + params.k_plus_3);
    *(e_new) = 2.0 * (top / bottom) - e_old;
}

Real Modeller::calc_I(Real *p)
{
    Real result = 0.0;
    const Real Dp0 = Dp(0.0);
    Real tmp = 4.0 * p[1] - p[2];
    if (tmp >= 0.0)
        result = params.ne * F * Dp(0) * tmp / (2.0 * h());
    else
        result = params.ne * F * Dp0 * p[1] / h();

    if (result < 0.0)
    {
        printf("Srovės tankis i(t) nėra monotoniškai didėjantis laiko t atžvilgiu. Nepavyko įvertinti biojutiklio atsako I.");
        exit(-1);
    }
    return result > __DBL_EPSILON__ ? result : 0.0;
}

Real Modeller::calc_dI(const Real v, const Real *x, const Real *u)
{
    Real a = (v + v - x[1] - x[2]) / ((x[0] - x[1]) * (x[0] - x[2]));
    Real b = (v + v - x[0] - x[2]) / ((x[1] - x[0]) * (x[1] - x[2]));
    Real c = (v + v - x[0] - x[1]) / ((x[2] - x[0]) * (x[2] - x[1]));
    Real result = a * u[0] + b * u[1] + c * u[2];
    return result;
}

void Modeller::printToCSV()
{
    //    if (_dI >= 1e-9)
    //    {
    //        if (last_i == 0.0)
    //        {
    //            last_i = _I;
    //        }
    //        else if (fabs(last_i - _I) <= DBL_EPSILON * 100.0 || fabs(last_di - _dI) <= DBL_EPSILON * 100.0)
    //        {
    //            return;
    //        }
    //        else
    //        {
    //            last_i = _I;
    //            last_di = _dI;
    //        }
    //
    //        if (last_di == 0.0)
    //        {
    //            last_di = _dI;
    //        }
    //
    //        fprintf(fo, "%.30lf;%.30lf\n", t, _I);
    //        fprintf(fo2, "%.30lf;%.30lf\n", t, _dI);
    //    }
}
