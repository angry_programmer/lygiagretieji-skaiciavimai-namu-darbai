//
//  main.c
//  p1_weighting
//
//  Created by Gintaras on 04/01/15.
//  Copyright (c) 2015 Gintaras Dreizas. All rights reserved.
//

#define MAIN_FILE
#include "parameters.h"
#include "modelling.h"
#include <signal.h>
#include <stdlib.h>
#include <string.h>
#include <float.h>
#include <stdint.h>
#include <iostream>
#include <exception>
#include <chrono>
#include <vector>
#include <sstream>
#include "csvfile.hpp"

#ifdef DISABLE_INLINE
#define MAYBE_INLINE __attribute__((noinline))
#else
#define MAYBE_INLINE
#endif

#ifdef DISABLE_INLINE
#define FORCE_INLINE
#else
#define FORCE_INLINE __attribute__((always_inline))
#endif

static const char BASE_PATH[32] = "./output";

std::string run_name;
std::string output_path;
bool include_prod_diff_out_calculations;
size_t vector_len;
size_t rep_count = 1;
size_t nthreads = 1;

typedef std::chrono::duration<unsigned long long, std::micro> microseconds;
typedef std::chrono::duration<unsigned long long, std::pico> picoseconds;
typedef std::chrono::duration<double, typename std::chrono::high_resolution_clock::period> Cycle;
using std::chrono::duration_cast;

struct bench_ret
{
    uint64_t avg;   /* avg runtime of a call to fn */
    uint64_t total; /* total runtime of calling fn iteration times */
    uint64_t loopfn_total;
};

static Cycle loopfn(size_t it, size_t nthreads, void (*fn)()) FORCE_INLINE;
static Cycle loopfn(size_t it, size_t nthreads, void (*fn)())
{
    printf("Starting loopfn with %2zu threads for %2zu iterations.\n", nthreads, it);
    auto t0 = std::chrono::high_resolution_clock::now();
#pragma omp parallel num_threads(nthreads)
    {
#pragma omp for
        for (size_t i = 0; i < it; i++)
        {
            printf("[%2zu|%2zu] started\n", i, it);
            fn();
            printf("[%2zu|%2zu] finished\n", i, it);
        }
    }
    auto t1 = std::chrono::high_resolution_clock::now();
    auto ticks = Cycle(t1 - t0);
    return ticks;
}

static struct bench_ret benchfn(size_t it, size_t nthreads, void (*fn)(), const char *name) FORCE_INLINE;
static struct bench_ret benchfn(size_t it, size_t nthreads, void (*fn)(), const char *name)
{
    printf("Pradedamas bandymas %-25s\n", name);
    auto start = std::chrono::high_resolution_clock::now();
    auto loopfn_says = loopfn(it, nthreads, fn);
    auto total = std::chrono::high_resolution_clock::now() - start;
    auto avg = total / it;

    auto total_ps = duration_cast<picoseconds>(total).count();
    auto avg_ps = duration_cast<picoseconds>(avg).count();
    auto loopfn_says_ps = duration_cast<picoseconds>(loopfn_says).count();
    printf("%-25s took %20llu ps, to run %zu iterations %20llu ps per call, loopfn_says %llu ps\n", name, total_ps, it, avg_ps, loopfn_says_ps);
    return (struct bench_ret){.avg = avg_ps, .total = total_ps, .loopfn_total = loopfn_says_ps};
}

void runner_fn();
void modelling_fn(size_t varIndex);

void segfault_sigaction(int signal, siginfo_t *si, void *arg)
{
    printf("Caught segfault at address %p\n", si->si_addr);
    exit(0);
}

/**
     * Params:
     * [0] - program name, unused
     * [1] - input file for parameters boundaries
     * [2] - output folder name (experiment codename)
     * [3] - defines whether or do not handle product
     diffusion outside biosensor throught membrane m2
     * [4] - N, vector and matrix size parameter
     * [5] - rep, number of repetitions for the same run
     * [6] - nthreads, number of OpenMP threads
     */
int main(int argc, const char *argv[])
{
    if (argc < 4)
    {
        fprintf(stderr, "Nenurodyti visi PRIVALOMI programos parametrai.\n");
        for (size_t i = 0; i < (size_t)argc; i++)
        {
            fprintf(stderr, "argv[%03zu]=%s\n", i, argv[i]);
        }
        exit(EXIT_FAILURE);
    }

    printf("Programa %-25s pradeda darba.\n", argv[2]);
    printf("Paradedamas %u pateiktu argumentu nuskaitymas baigtas\n", argc);

    load_bounds(argv[1]);
    set_var_percent("k+3", 0.038);

    run_name = argv[2];
    output_path = std::string(BASE_PATH);
    {
        char shell_cmd[256];
        sprintf(shell_cmd, "mkdir -p %s", BASE_PATH);
        system(shell_cmd);
    }

    include_prod_diff_out_calculations = atoi(argv[3]);

    if (argc > 4)
    {
        params.N = atoi(argv[4]);
    }

    if (params.N < 1000)
    {
        fprintf(stderr, "Vektoriaus ilgis privalo buti >= 1000\n");
        exit(EXIT_FAILURE);
    }

    if (argc > 5)
    {
        rep_count = atoi(argv[5]);
    }

    if (argc > 6)
    {
        nthreads = atoi(argv[6]);
    }

    printf("Pateiktu argumentu nuskaitymas baigtas\n");

    struct sigaction sa;

    memset(&sa, 0, sizeof(struct sigaction));
    sigemptyset(&sa.sa_mask);
    sa.sa_sigaction = segfault_sigaction;
    sa.sa_flags = SA_SIGINFO;

    sigaction(SIGSEGV, &sa, NULL);
    try
    {
        struct bench_ret bench = benchfn(rep_count, nthreads, runner_fn, "benchmark");

        std::stringstream ss;
        ss << output_path << '/' << run_name << "-benchmark.csv";
        std::string csvname = ss.str();
        bool file_exist = false;
        {
            std::ifstream f;
            f.open(csvname, std::ios_base::binary);
            f.seekg(0, std::ios_base::end);
            std::ifstream::pos_type size = f.tellg();
            f.close();
            file_exist = size > 0;
        }
        csvfile csv(csvname);
        if (!file_exist)
        {
            // Header
            csv << "p"
                << "it"
                << "N"
                << "prod diff"
                << "total [ps]"
                << "avg [ps]"
                << "loopfn [ps]"
                << endrow;
        }
        // Data
        csv << nthreads << rep_count << params.N << include_prod_diff_out_calculations << bench.total << bench.avg << bench.loopfn_total << endrow;
    }
    catch (std::exception &e)
    {
        std::cerr << "Exception catched : " << e.what() << std::endl;
    }

    printf("Programa %-25s sekmingai baige darba.\n", argv[2]);
    return EXIT_SUCCESS;
}

void runner_fn()
{
    Real result[2];
    Modeller *modeller = new Modeller(include_prod_diff_out_calculations);
    modeller->run(output_path.c_str(), result);
    delete modeller;
}
