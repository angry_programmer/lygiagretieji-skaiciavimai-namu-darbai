//
//  main.c
//  p1_weighting
//
//  Created by Gintaras on 04/01/15.
//  Copyright (c) 2015 Gintaras Dreizas. All rights reserved.
//

#define MAIN_FILE
#include "parameters.h"
#include "modelling.h"
#include <stdlib.h>
#include <string.h>
#include <float.h>
#include <stdint.h>
#include <iostream>
#include <exception>
#include <chrono>
#include <vector>
#include <sstream>
#include <algorithm>
#include <mpi.h>
#include "csvfile.hpp"

#ifdef DISABLE_INLINE
#define MAYBE_INLINE __attribute__((noinline))
#else
#define MAYBE_INLINE
#endif

#ifdef DISABLE_INLINE
#define FORCE_INLINE
#else
#define FORCE_INLINE __attribute__((always_inline))
#endif

static const char BASE_PATH[32] = "./output";

std::string run_name;
std::string output_path;
bool include_prod_diff_out_calculations;
size_t vector_len;
size_t rep_count = 1;

typedef std::chrono::duration<unsigned long long, std::micro> microseconds;
typedef std::chrono::duration<unsigned long long, std::pico> picoseconds;
typedef std::chrono::duration<double, typename std::chrono::high_resolution_clock::period> Cycle;
using std::chrono::duration_cast;

typedef struct
{
    char name[MPI_MAX_PROCESSOR_NAME];
    int name_len;
    int rank;
    int size;
} world_t;

typedef struct
{
    char var[16];
    size_t displ;
    size_t count;
} cmd_t;

struct bench_ret
{
    uint64_t avg;   /* avg runtime of a call to fn */
    uint64_t total; /* total runtime of calling fn iteration times */
    uint64_t loopfn_total;
};

static Cycle loopfn(size_t displ, size_t count, void (*fn)()) FORCE_INLINE;
static Cycle loopfn(size_t displ, size_t count, void (*fn)())
{
    auto t0 = std::chrono::high_resolution_clock::now();
    for (size_t i = 0; i < count; i++)
    {
        printf("[%2zu|%2zu] started\n", i + displ, count + displ);
        fn();
        printf("[%2zu|%2zu] finished\n", i + displ, count + displ);
    }
    auto t1 = std::chrono::high_resolution_clock::now();
    auto ticks = Cycle(t1 - t0);
    return ticks;
}

static struct bench_ret benchfn(world_t *world, size_t it, void (*fn)(), const char *name) FORCE_INLINE;
static struct bench_ret benchfn(world_t *world, size_t it, void (*fn)(), const char *name)
{
    printf("Pradedamas bandymas %s\n", name);
    auto start = std::chrono::high_resolution_clock::now();
    int *scounts = (int *)malloc(sizeof(int) * world->size);
    int *displs = (int *)malloc(sizeof(int) * world->size);
    if (world->rank == 0)
    {
        size_t block_size = it / world->size;
        size_t tails = it % world->size;
        size_t take = 0;
        size_t index = 0;
        for (size_t i = 0; i < it; i += take)
        {
            if (tails > 0 && i > 0)
            {
                tails--;
                take = block_size + 1;
            }
            else
            {
                take = block_size;
            }
            int displ = std::min(it, i);
            displs[index] = displ;
            scounts[index] = take;
            index++;
        }

        for (size_t i = 0; i < world->size; i++)
        {
            printf("%s[%03zu|%03d]. scount=%03d\t displ=%03d.\n", world->name, i, world->size, scounts[i], displs[i]);
        }
    }

    // printf("%s [%03d|%03d]. Before MPI_Scatter.\n", world.name, world.rank, world.size);
    size_t count_in_proc = 0;
    size_t displ_in_proc = 0;
    MPI_Scatter(scounts, 1, MPI_UNSIGNED, &count_in_proc, 1, MPI_UNSIGNED, 0, MPI_COMM_WORLD);
    MPI_Scatter(displs, 1, MPI_UNSIGNED, &displ_in_proc, 1, MPI_UNSIGNED, 0, MPI_COMM_WORLD);

    auto loopfn_says = loopfn(displ_in_proc, count_in_proc, fn);
    auto loopfn_says_ps = duration_cast<picoseconds>(loopfn_says).count();

    bench_ret ret = {0};
    if (world->rank == 0)
    {
        for (size_t r = 1; r < world->size; r++)
        {
            uint64_t proc_loopfn_says_ps = 0;
            ;
            MPI_Recv(&proc_loopfn_says_ps, 1, MPI_UNSIGNED_LONG_LONG, (int)r, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            loopfn_says_ps += proc_loopfn_says_ps;
            size_t count = scounts[world->rank];
            auto avg_ps = proc_loopfn_says_ps / count;
            printf("[%03zu|%03d]%s took %20llu ps, to run %zu iterations %20llu ps per call\n", r, world->size, name, loopfn_says_ps, count, avg_ps);
        }

        auto total = std::chrono::high_resolution_clock::now() - start;
        auto avg = total / it;
        auto total_ps = duration_cast<picoseconds>(total).count();
        auto avg_ps = duration_cast<picoseconds>(avg).count();
        printf("%s took %20llu ps, to run %zu iterations %20llu ps per call, loopfn_says %llu ps\n", name, total_ps, it, avg_ps, loopfn_says_ps);
        ret = (struct bench_ret){.avg = avg_ps, .total = total_ps, .loopfn_total = loopfn_says_ps};
    }
    else
    {
        MPI_Send(&loopfn_says_ps, 1, MPI_UNSIGNED_LONG_LONG, 0, 0, MPI_COMM_WORLD);
    }

    return ret;
}

void runner_fn();
void modelling_fn(size_t varIndex);

/**
 * Params:
 * [0] - program name, unused
 * [1] - input file for parameters boundaries
 * [2] - output folder name (experiment codename)
 * [3] - defines whether or do not handle product
 diffusion outside biosensor throught membrane m2
 * [4] - N, vector and matrix size parameter
 * [5] - rep, number of repetitions for the same run
 */
int main(int argc, char *argv[])
{
    MPI_Init(&argc, &argv);

    world_t world;
    MPI_Comm_size(MPI_COMM_WORLD, &world.size);
    MPI_Comm_rank(MPI_COMM_WORLD, &world.rank);
    MPI_Get_processor_name(world.name, &world.name_len);

    if (argc < 4)
    {
        fprintf(stderr, "Nenurodyti visi PRIVALOMI programos parametrai.\n");
        exit(EXIT_FAILURE);
    }

    printf("Programa %s pradeda darba.\n", argv[2]);
    printf("Paradedamas %u pateiktu argumentu nuskaitymas baigtas\n", argc);

    load_bounds(argv[1]);
    set_var_percent("k+3", 0.038);

    run_name = argv[2];
    output_path = std::string(BASE_PATH);
    if (world.rank == 0)
    {
        char shell_cmd[256];
        sprintf(shell_cmd, "mkdir -p %s", BASE_PATH);
        system(shell_cmd);
    }

    include_prod_diff_out_calculations = atoi(argv[3]);

    if (argc > 4)
    {
        params.N = atoi(argv[4]);
    }

    if (params.N < 1000)
    {
        fprintf(stderr, "Vektoriaus ilgis privalo buti >= 1000\n");
        exit(EXIT_FAILURE);
    }

    if (argc > 5)
    {
        rep_count = atoi(argv[5]);
    }

    printf("Pateiktu argumentu nuskaitymas baigtas\n");

    MPI_Barrier(MPI_COMM_WORLD);
    try
    {
        bench_ret bench = benchfn(&world, rep_count, runner_fn, run_name.c_str());

        if (world.rank == 0)
        {
            std::stringstream ss;
            ss << output_path << '/' << run_name << "-benchmark.csv";
            std::string csvname = ss.str();
            bool file_exist = false;
            {
                std::ifstream f;
                f.open(csvname, std::ios_base::binary);
                f.seekg(0, std::ios_base::end);
                std::ifstream::pos_type size = f.tellg();
                f.close();
                file_exist = size > 0;
            }
            csvfile csv(csvname);
            if (!file_exist)
            {
                // Header
                csv << "p"
                    << "it"
                    << "N"
                    << "prod diff"
                    << "total [ps]"
                    << "avg [ps]"
                    << "loopfn [ps]"
                    << endrow;
            }
            // Data
            csv << world.size << rep_count << params.N << include_prod_diff_out_calculations << bench.total << bench.avg << bench.loopfn_total << endrow;
        }
    }
    catch (std::exception &e)
    {
        fprintf(stderr, "%s[%03d|%03d]. Exception catched : %s\n", world.name, world.rank, world.size, e.what());
    }

    printf("%s[%03d|%03d]. Programa %s sekmingai baige darba.\n", world.name, world.rank, world.size, argv[2]);
    MPI_Barrier(MPI_COMM_WORLD);
    MPI_Finalize();
    return EXIT_SUCCESS;
}

void runner_fn()
{
    Real result[2];
    Modeller *modeller = new Modeller(include_prod_diff_out_calculations);
    modeller->run(output_path.c_str(), result);
}
