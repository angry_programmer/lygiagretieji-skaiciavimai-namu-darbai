//
//  main.c
//  p1_weighting
//
//  Created by Gintaras on 04/01/15.
//  Copyright (c) 2015 Gintaras Dreizas. All rights reserved.
//

#define MAIN_FILE
#include "parameters.h"
#include "modelling.h"
#include <stdlib.h>
#include <string.h>
#include <float.h>
#include <mpi.h>

static const char BASE_PATH[32] = "results/weighting";
static const int N_VARS = 4;
static const char * VARS[] = {
    "dm1",
    "dm2",
    "de",
    "DPm1",
    // "DPm2",
    // "DPe",
    // "DSm1",
    // "DSm2",
    // "DSe",
    // "k+1",
    // "k-1",
    // "k+3",
};

typedef struct {
    char name[MPI_MAX_PROCESSOR_NAME];
    int name_len;
    int rank;
    int size;
} world_t;

typedef struct {
    char var[8];
    Real value;
    Real results[2][2];
} cmd_t;

size_t iterations_count;
Real percent_per_iteration;

void runModellingForVar(size_t varIndex);

int main(int argc, const char * argv[]) {
    world_t world;
    MPI_Init(&argc, &argv);

    /**
    * Create cmd MPI_Datatype
    */
    MPI_Datatype mpi_cmd_t;
    {
        cmd_t cmd;
        int blockcounts[2] = {sizeof(char) * 8, 1 + 2 * 2};
        MPI_Datatype types[2] = {MPI_CHAR, MPI_DOUBLE};
        MPI_Aint displs[2];

        MPI_Get_address(&cmd.var, &displs[0]);
        MPI_Get_address(&cmd.value, &displs[1]);
        for (int i = 1; i >= 0; i--) {
            displs[i] -= displs[0];
        }

        MPI_Type_create_struct(2, blockcounts, displs, types, &mpi_cmd_t);
        MPI_Type_commit(&mpi_cmd_t);
    }
    /**
    * Create cmd MPI_Datatype
    */

    MPI_Comm_size(MPI_COMM_WORLD, &world.size);
    MPI_Comm_rank(MPI_COMM_WORLD, &world.rank);
    MPI_Get_processor_name(world.name, &world.name_len);

    if (argc < 2) {
        fprintf(stderr, "Nenurodytas pradinis skaicius.");
        exit(EXIT_FAILURE);
    }
    
    iterations_count = atoi(argv[1]);
    if (iterations_count > 1000) {
        fprintf(stderr, "Abejoju ar norite taip ilgai laukti modeliavimo rezultatu.");
        exit(EXIT_FAILURE);
    }
    percent_per_iteration = 1.0 / (Real) iterations_count;
    
    if (argc < 3) {
        fprintf(stderr, "Nenurodytas parametru ribas apibrezintis duomenu failas.");
        exit(EXIT_FAILURE);
    }
    load_bounds(argv[2]);

    printf("%s [%03d|%03d]. Pradeda darba.\n", world.name, world.rank, world.size);

    size_t cmds_count = (iterations_count + 1) * N_VARS;
    printf("%s [%03d|%03d]. cmds_count=%zu.\n", world.name, world.rank, world.size, cmds_count);
    cmd_t *cmds = malloc(sizeof(cmd_t) * cmds_count);
    int *scounts = malloc(sizeof(int) * world.size);
    int *displs = malloc(sizeof(int) * world.size);
    if (world.rank == 0) {
        for (size_t var_index = 0; var_index < N_VARS; var_index++) {
            Real value = 0.0;
            for (size_t it_index = 0; it_index <= iterations_count; it_index++) {
                size_t index = var_index * (iterations_count + 1) + it_index;
                cmd_t *cmd = &cmds[index];
                sprintf(cmd->var, "%s", VARS[var_index]);
                cmd->value = (it_index + 0.0) / (Real) iterations_count;
            }
        }

        size_t block_size = cmds_count / world.size;
        size_t tails = cmds_count % world.size;
        printf("%s [%03d|%03d]. block_size=%zu\ttails=%zu.\n", world.name, world.rank, world.size, block_size, tails);
        size_t take = 0;
        size_t index = 0;
        for (size_t i = 0; i < cmds_count; i += take) {
            if (tails > 0 && i > 0) {
                tails--;
                take = block_size + 1;
            } else {
                take = block_size;
            }
            int displ = min(cmds_count, i);
            printf("%s [%03d|%03d]. displ=min(cmds_count=%zu, i=%zu + take=%zu) = %d.\n", world.name, index, world.size, cmds_count, i, take, displ);
            displs[index] = displ;
            scounts[index] = take;
            index++;
        }

        for (size_t i = 0; i < world.size; i++) {
            printf("%s [%03d|%03d]. scount=%d\t displ=%d.\n", world.name, i, world.size, scounts[i], displs[i]);
        }
    }

    printf("%s [%03d|%03d]. Before MPI_Scatter.\n", world.name, world.rank, world.size);
    int local_rcount = 0;
    MPI_Scatter(scounts, 1, MPI_INT, &local_rcount, 1, MPI_INT, 0, MPI_COMM_WORLD);
    printf("%s [%03d|%03d]. After MPI_Scatter(%d).\n", world.name, world.rank, world.size, local_rcount);
    cmd_t local_cmds[local_rcount];
    MPI_Scatterv(cmds, scounts, displs, mpi_cmd_t, local_cmds, local_rcount, mpi_cmd_t, 0, MPI_COMM_WORLD);

    int     text_len;
    char    text[MPI_MAX_PROCESSOR_NAME];
    if (world.rank == 0) {
        for (size_t i = 0; i < local_rcount; i++) {
            printf("%s [%03d|%03d]. Gavau komanda: %s:%.4f\n", world.name, world.rank, world.size, local_cmds[i].var, local_cmds[i].value);
        }
        for (size_t r = 1; r < world.size; r++) {
            for (size_t foo = 0; foo < scounts[r]; foo++) {
                MPI_Recv(&text_len, 1, MPI_INT, r, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
                MPI_Recv(text, text_len+1, MPI_CHAR, r, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
                printf("%s\n", text);
            }
            printf("\n");
        }
    } else {
        for (size_t i = 0; i < local_rcount; i++) {
            sprintf(text, "%s [%03d|%03d]. Gavau komanda: %s:%.4f", world.name, world.rank, world.size, local_cmds[i].var, local_cmds[i].value);
            text_len = strlen(text);
            MPI_Send(&text_len, 1, MPI_INT, 0, 0, MPI_COMM_WORLD);
            MPI_Send(text, text_len+1, MPI_CHAR, 0, 1, MPI_COMM_WORLD);
            // printf("%s [%03d|%03d]. Gavau komanda: %s:%.4f\n", world.name, world.rank, world.size, local_cmds[i].var, local_cmds[i].value);
        }
    }
    
    // Real result[2][2];
    // char controlPath[64];
    // sprintf(controlPath, "%s/kontrole", BASE_PATH);
    // run_modelling(controlPath, 0.5, 0, result);



    // char filename[48];
    // sprintf(filename, "%s/raw.csv", controlPath);
    // FILE *inner = fopen(filename, "w");
    // fprintf(inner, "ndiff_t;ndiff_I;tdiff_t;tdiff_I\n");
    // fprintf(inner, "%.6lf;%.6lf;%.6lf;%.6lf\n", result[0][0], result[0][1], result[1][0], result[1][1]);
    // fclose(inner);
    
    // for (size_t varIndex = 0; varIndex < N_VARS; varIndex++) {
    //     printf("%lu of %d\n", (varIndex + 1), N_VARS);
    //     runModellingForVar(varIndex);
    // }
    MPI_Barrier(MPI_COMM_WORLD);
    free(cmds);
    free(scounts);
    free(displs);
    MPI_Finalize();
    return EXIT_SUCCESS;
}

void runModellingForVar(size_t varIndex) {
    
    const char * param = VARS[varIndex];
    
    char rpath[64];
    sprintf(rpath, "%s/%s", BASE_PATH, param);
    {
            char shell_cmd[256];
            sprintf(shell_cmd, "mkdir -p %s", rpath);
            system(shell_cmd);
        }
    Real percent = 0.0;
    int counter = 0;
    char filename[32];
    sprintf(filename, "%s/raw.csv", rpath);
    FILE *inner = fopen(filename, "w");
    if (inner == NULL) {
        printf("inner = NULL\n");
        exit(EXIT_FAILURE);
    }
    fprintf(inner, "pct;ndiff_t;ndiff_I;tdiff_t;tdiff_I\n");

    do {
        Real result[2][2];
        Real value = set_var_percent(param, percent);
        run_modelling(rpath, value, counter, result);
        fprintf(inner, "%.6lf;%.6lf;%.6lf;%.6lf;%.6lf\n", percent, result[0][0], result[0][1], result[1][0], result[1][1]);
        counter++;
        percent += percent_per_iteration;
        if (percent == 0.5) {
            percent += percent_per_iteration;
        }
    } while (1.0 >= percent);
    fclose(inner);
    set_var_percent(param, 0.5);
    
    // daliniti is vidurines reiksmes.
//    fprintf(out, "%s;%.6lf;%.6lf;%.6lf;%.6lf\n",
//            param,
//            finalResult[0][0] / (Real) (counter - 1),
//            finalResult[0][1] / (Real) (counter - 1),
//            finalResult[1][0] / (Real) (counter - 1),
//            finalResult[1][1] / (Real) (counter - 1)
//            );
    
}
