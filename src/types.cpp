//
//  types.c
//  MBD-II
//
//  Created by Gintaras on 16/10/14.
//  Copyright (c) 2014 Gintaras Dreizas. All rights reserved.
//

#include "types.h"
#include <sys/time.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <float.h>

const err ERR_NULL = 0;
const err ERR_UNABLE_TO_OPEN_FILE = -1;

Real sqr(const Real x)
{
    return (x * x);
}

Real Vec4_len(const Vec4 v)
{
    double pa = pow(v[0], 2.0);
    double pb = pow(v[1], 2.0);
    double pc = pow(v[2], 2.0);
    double pd = pow(v[3], 2.0);
    double r = sqrt(pa + pb + pc + pd);
    return r;
}

void Vec4_init(
               Vec4 r,
               Real v
               )
{
    r[0] = v;
    r[1] = v;
    r[2] = v;
    r[3] = v;
}

void Vec4_cpy(const Vec4 from, Vec4 to)
{
    to[0] = from[0];
    to[1] = from[1];
    to[2] = from[2];
    to[3] = from[3];
}

void Vec4_sum(
              const Vec4 a,
              const Vec4 b,
              Vec4 r
              )
{
    r[0] = a[0] + b[0];
    r[1] = a[1] + b[1];
    r[2] = a[2] + b[2];
    r[3] = a[3] + b[3];
}

void Vec4_mul(
              const Vec4 a,
              const Vec4 b,
              Vec4 r
              )
{
    r[0] = a[0] * b[0];
    r[1] = a[1] * b[1];
    r[2] = a[2] * b[2];
    r[3] = a[3] * b[3];
}

Real angle_between(
                   const Real x1,
                   const Real x2,
                   const Real y1,
                   const Real y2
                   )
{
    Real a = x1 * x2 + y1 * y2;
    Real b = sqrt(x1 * x1 + y1 * y1) * sqrt(x2 * x2 + y2 * y2);
    Real c = a / b;
    return c;
}

void arr_cpy(
             Real **dst,
             Real **src,
             size_t len
             ) {
    size_t tmpsz = sizeof(*src) * len;
    if ((*dst != NULL) || (*dst = (Real *) malloc(tmpsz)) != NULL)
    {
        memcpy(*dst, *src, tmpsz);
    }
}

Real find_max(
              const Real *a,
              const size_t n
              )
{
    Real m = 0.0;
    for (size_t i = 0; i < n; i++) {
        m = (a[i] > m) ? a[i] : m;
    }
    return m;
}

Real least_squares(
                   const Real* a,
                   const Real* b,
                   const size_t n
                   )
{
    Real lsq = 0.0;
    for (size_t i = 0; i < n; i++) {
        lsq += sqr(fabs(a[i] - b[i]));
    }
    return lsq;
}

void err_message(
                 const err e,
                 char* m
                 )
{
    if (e == ERR_UNABLE_TO_OPEN_FILE) {
        sprintf(m, "Klaida atidarant bylą.\n");
    }
}

unsigned long long getTimestamp() {
    struct timeval tv;
    gettimeofday(&tv,NULL);
    return (unsigned long long)(tv.tv_sec * 1000000llu + tv.tv_usec);
}
