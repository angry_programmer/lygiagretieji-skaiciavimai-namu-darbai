//
//  modelling.h
//  MBD-II
//
//  Created by Gintaras on 04/01/15.
//  Copyright (c) 2015 Gintaras Dreizas. All rights reserved.
//

#ifndef MBD_II_modelling_h
#define MBD_II_modelling_h

#include <assert.h>
#include <time.h>
#include "parameters.h"
#include "results.h"

class Modeller
{

  bool prod_diffusion_through_outer_membrane;

  Real *s_prev;
  Real *p_prev;
  Real *e_prev;

  Real *s_array;
  Real *p_array;
  Real *e_array;

  Real *s_new;
  Real *p_new;
  Real *e_new;

  Real *s_old;
  Real *p_old;
  Real *e_old;

  Real *alpha;
  Real *beta;

  Real *a;
  Real *b;
  Real *c;
  Real *f;
  Real *z;
  Real *A;
  Real *B;
  Real *Y;

  Real tau_prev;
  Real tau;
  Real tau_next;

  Real t;

  Real max_dS_dt_prev;
  Real max_dS_dt_curr;

  long k;
  Real t_slopeMAX;

  Real _I_prev;
  Real _I_prev2;
  Real _I;

  Real _dI_prev;
  Real _dI_prev2;
  Real _dI;

  Real max_dI;

  FILE *fo;
  FILE *fo2;

  Real last_i;
  Real last_di;

  char current_path[256];

public:
  Modeller();
  Modeller(bool prod_diffusion_through_outer_membrane);
  ~Modeller();

  void run(const char *base_path, Real result[2]);

private:
  size_t _array_sz;
  void _init_arrays();
  void _clean_up_arrays();
  void _destroy_arrays();

  void _clean_up_prev_arrays();
  void _clean_up_current_arrays();
  void _clean_up_new_arrays();
  void _clean_up_old_arrays();
  void _clean_up_abcfz_arrays();
  void _clean_up_ABY_arrays();
  void _clean_up_tdma_arrays();

  void solve_system();
  void solve_system_S(Real *e_old, Real *new_array);
  void solve_system_P(Real *e_old, Real *new_array);

  void tdma(Real *a, Real *b, Real *c, Real *f, Real r0, Real rn, Real *result);
  void tdma2(Real *a, Real *b, Real *c, Real *f, Real *result);

  void shift_tau_values();
  void shift_tau_values2();
  void shift_I_values();
  void shift_dI_values();
  void shift_arrays();

  Real S(size_t i);
  Real P(size_t i);
  Real E(size_t i);

  Real S_c(Real e_old, size_t i);
  Real S_f(Real e_old, size_t i);

  Real P_c(Real e_old, size_t i);
  Real P_f(Real e_old, size_t i);

  void E_(Real *e_new, Real e_old, Real s_old, Real s_new, Real p_old, Real p_new, size_t i);
  Real calc_I(Real *p);
  Real calc_dI(const Real v, const Real *x, const Real *u);
  void printToCSV();
};

#endif
