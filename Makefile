TARGET=./bin/biosensor

SOURCEDIR=./src
# SOURCE = $(SOURCEDIR)/config.cpp

## End sources definition
INCLUDE =-I/usr/local/include  
# INCLUDE = -I.$(SOURCEDIR)/../inc
## end more includes

# LIBS=-L/usr/local/lib -lboost_serialization-mt
prod_diff := false true
n_lengths := 1000 1259 1585 1995 2512 3162 3981 5012 6310 7943 10000

CONST_LOCAL_HOSTNAME=Gintarass-MacBook-Pro.local Gintarass-MBP
HOSTNAME=$(shell hostname)
ifneq ($(filter $(HOSTNAME),$(CONST_LOCAL_HOSTNAME)),)
	CC=g++-7
	CFLAGS=-std=c++11 -Wall -O3 -funroll-loops -mtune=native -fopenmp
	RUN_CMD=${TARGET} ./ribos omp
	SOURCE=$(SOURCEDIR)/main.cpp $(SOURCEDIR)/modelling.cpp $(SOURCEDIR)/parameters.cpp $(SOURCEDIR)/results.cpp $(SOURCEDIR)/types.cpp
	n_threads := 1 2 4 8
else
	CC=mpic++
	CFLAGS=-std=c++11 -Wall -O3 -funroll-loops -mtune=native
	RUN_CMD=mpirun $(TARGET) ./ribos mpi
	SOURCE=$(SOURCEDIR)/main-cluster.cpp $(SOURCEDIR)/modelling.cpp $(SOURCEDIR)/parameters.cpp $(SOURCEDIR)/results.cpp $(SOURCEDIR)/types.cpp
	n_threads := 1
endif

VPATH=$(SOURCEDIR)
OBJ=$(join $(addsuffix ../obj/, $(dir $(SOURCE))), $(notdir $(SOURCE:.cpp=.o))) 

## Fix dependency destination to be ../.dep relative to the src dir
DEPENDS=$(join $(addsuffix ../.dep/, $(dir $(SOURCE))), $(notdir $(SOURCE:.cpp=.d)))

a:
	@for file in 536719 536729 536744 536749 536755 536760 536761 536763 536780 536784 536787 536731 536785 536781 536777 536766 536757 536753 536738 536732 536725 536723 536794 536788 536783 536776 536774 536769 536759 536745 536741 536736 536746 536751 536765 536778 536786 536790 536792 536728 536722 536720 536758 536764 536773 536775 536782 536793 536756 536752 536718 536721 536726 536730 536733 536734 536735 536737 536743 536747 536748 536750 536754 536762 536767 536768 536770 536771 536772 536779 536789 536791; \
	do \
		scancel $$file; \
	done;
	@true

## Default rule executed
all: $(TARGET)
	@true

## Info rule
info:
	@echo "============="
	@echo "Printing target ${TARGET} run info"
	@echo "============="
	@echo mpirun ${RUN_FLAGS} ./${TARGET} 1 ./ribos

## Clean Rule
clean:
	@-rm -f $(TARGET) $(OBJ) $(DEPENDS)

## Run rule
run:
	@echo "============="
	@echo "Running target ${TARGET}"
	@echo "============="
	$(RUN_CMD) $(ARGS)
	@true

## Run rule for OpenMP
run-omp:
	@echo "============="
	@echo "Running target ${TARGET} with OpenMP"
	@echo "============="
	@for diff in $(prod_diff); \
	do \
		for n in $(n_lengths); \
		do \
			for nthreads in $(n_threads); \
			do \
				$(RUN_CMD) $$diff $$n 64 $$nthreads; \
			done; \
		done; \
	done;
	@true

## Rule for making the actual target
$(TARGET): $(OBJ)
	@echo "============="
	@echo "Linking the target $@"
	@echo "============="
	@mkdir -p $(dir $@)
	@$(CC) $(CFLAGS) -o $@ $^ $(LIBS)
	@echo -- Link finished --

## Generic compilation rule
%.o : %.cpp
	@mkdir -p $(dir $@)
	@echo "============="
	@echo "Compiling $<"
	@$(CC) $(CFLAGS) -c $< -o $@


# Rules for object files from c files
# Object file for each file is put in obj directory
# one level up from the actual source directory.
../obj/%.o : %.cpp
	@mkdir -p $(dir $@)
	@echo "============="
	@echo "Compiling $<"
	@$(CC) $(CFLAGS) -c $< -o $@

# Rule for "other directory"  You will need one per "other" dir
$(SOURCEDIR)/../obj/%.o : %.cpp
	@mkdir -p $(dir $@)
	@echo "============="
	@echo "Compiling $<"
	@$(CC) $(CFLAGS) ${INCLUDE} -c $< -o $@

# Make dependancy rules
../.dep/%.d: %.cpp
	@mkdir -p $(dir $@)
	@echo "============="
	@echo Building dependencies file for $*.o
	@$(SHELL) -ec '$(CC) -M $(CFLAGS) $< | sed "s^$*.o^../obj/$*.o^" > $@'

## Dependency rule for "other" directory
$(SOURCEDIR)/../.dep/%.d: %.cpp
	@mkdir -p $(dir $@)
	@echo "============="
	@echo Building dependencies file for $*.o
	@$(SHELL) -ec '$(CC) -M $(CFLAGS) $< | sed "s^$*.o^$(SOURCEDIR)/../obj/$*.o^" > $@'

## Include the dependency files
-include $(DEPENDS)